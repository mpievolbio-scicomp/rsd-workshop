{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setting up git\n",
    "\n",
    "Initially we need to set up git. \n",
    "\n",
    "git keeps track of the entire history of a project. This does not only mean keeping track of what was done but also who did it. So we start by telling git who we are by running the following two commands:\n",
    "\n",
    "```shell\n",
    "$ git config --global user.name \"Your Name\"\n",
    "$ git config --global user.email \"Your Email\"\n",
    "```\n",
    "\n",
    "**Note** this is not data that is being collected by any cloud service or similar. It just stays with your project.\n",
    "\n",
    "**Windows**\n",
    "Note that all these commands work on the anaconda prompt but if you want to use tab completion you can use the git bash command line specifically for git."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we are going to set [Nano](https://www.nano-editor.org) as the default editor for git. For unix you can use the following command:\n",
    "\n",
    "```shell\n",
    "$ git config --global core.editor \"nano\"\n",
    "```\n",
    "\n",
    "We are going to use Nano later in order to write a commit message. If you feel comfortable with another editor, e.g. emacs or vim, feel free to change the above\n",
    "`git config` command accordingly. The editor must be able to run inside the terminal window."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Configure your `git` installation as shown above. Set your name, email, and preferred editor.\n",
    "1. Lookup the documentation for the `git config`. Find out how to list the current global git configuration.\n",
    "1. List the current global git configuration and confirm that your user name, email and preferred editor are setup correctly.\n",
    "```\n",
    "\n",
    "## Solutions:\n",
    "1. Configure git\n",
    "```shell\n",
    "$ git config --global user.name \"First Last\"\n",
    "$ git config --global user.email \"last@evolbio.mpg.de\"\n",
    "```\n",
    "\n",
    "1. `git-config` documentation\n",
    "```shell\n",
    "$ git config -h\n",
    "```\n",
    "prints the usage instructions to the terminal:\n",
    "```\n",
    "    usage: git config [<options>]\n",
    "\n",
    "    Config file location\n",
    "        --global              use global config file\n",
    "        --system              use system config file\n",
    "        --local               use repository config file\n",
    "        --worktree            use per-worktree config file\n",
    "        -f, --file <file>     use given config file\n",
    "        --blob <blob-id>      read config from given blob object\n",
    "\n",
    "    Action\n",
    "        --get                 get value: name [value-regex]\n",
    "        --get-all             get all values: key [value-regex]\n",
    "        --get-regexp          get values for regexp: name-regex [value-regex]\n",
    "        --get-urlmatch        get value specific for the URL: section[.var] URL\n",
    "        --replace-all         replace all matching variables: name value [value_regex]\n",
    "        --add                 add a new variable: name value\n",
    "        --unset               remove a variable: name [value-regex]\n",
    "        --unset-all           remove all matches: name [value-regex]\n",
    "        --rename-section      rename section: old-name new-name\n",
    "        --remove-section      remove a section: name\n",
    "        -l, --list            list all\n",
    "        -e, --edit            open an editor\n",
    "        --get-color           find the color configured: slot [default]\n",
    "        --get-colorbool       find the color setting: slot [stdout-is-tty]\n",
    "\n",
    "    Type\n",
    "        -t, --type <>         value is given this type\n",
    "        --bool                value is \"true\" or \"false\"\n",
    "        --int                 value is decimal number\n",
    "        --bool-or-int         value is --bool or --int\n",
    "        --path                value is a path (file or directory name)\n",
    "        --expiry-date         value is an expiry date\n",
    "\n",
    "    Other\n",
    "        -z, --null            terminate values with NUL byte\n",
    "        --name-only           show variable names only\n",
    "        --includes            respect include directives on lookup\n",
    "        --show-origin         show origin of config (file, standard input, blob, command line)\n",
    "        --default <value>     with --get, use default value when missing entry\n",
    "```\n",
    "\n",
    "1. List the git configuration\n",
    "```shell\n",
    "$ git config --global --list\n",
    "user.name=Carsten Fortmann-Grote\n",
    "user.email=grotec@evolbio.mpg.de\n",
    "filter.lfs.clean=git-lfs clean -- %f\n",
    "filter.lfs.smudge=git-lfs smudge -- %f\n",
    "filter.lfs.process=git-lfs filter-process\n",
    "filter.lfs.required=true\n",
    "diff.tool=vimdiff\n",
    "core.editor=vim\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Initialising a git repository\n",
    "\n",
    "In order to demonstrate how version control with git works we are going to use the `rsd-workshop` folder we created before.\n",
    "\n",
    "We need tell git to start keeping an eye on this repository (folder/project). While in the `rsd-workshop` directory type:\n",
    "\n",
    "```shell\n",
    "$ git init\n",
    "```\n",
    "\n",
    "You should then see a message saying that you have successfully initialized a git repository.\n",
    "\n",
    "## Exercise\n",
    "1. Change into your directory `rsd-workshop` and initialize it as a git repository."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Staging and committing changes\n",
    "\n",
    "To see the status of the repository we just initialized type:\n",
    " \n",
    "```shell\n",
    "$ git status\n",
    "```\n",
    "\n",
    "We should see something like:\n",
    "\n",
    "![](../img/git_status.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are various pieces of useful information here, first of all that `addition.py`, `if-statement.py` and `while-loops.py` are not currently tracked files.\n",
    "\n",
    "We are now going to track the `addition.py` file:\n",
    "\n",
    "```shell\n",
    "$ git add addition.py\n",
    "```\n",
    "\n",
    "If we run git status again we see:\n",
    "\n",
    "![](../img/git_status_after_add.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have propagated our file from the \"Untracked\" to the \"Staged\" status.  \n",
    "![image.png](../img/staged.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the `addition.py` file is now ready to be \"committed\".\n",
    "\n",
    "```shell\n",
    "$ git commit\n",
    "```\n",
    "\n",
    "When doing this, a text editor should open up prompting you to write what is called a commit message. In our case the text editor that opens is Nano. \n",
    "\n",
    "For the purposes of using git Nano is more than a sufficient editor, all you need to know how to do is:\n",
    "\n",
    "- Write in Nano: just type;\n",
    "- Save in Nano: Ctrl + O;\n",
    "- Quit Nano: Ctrl + X.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Type the following as the first commit message:\n",
    "\n",
    "```shell\n",
    "Add addition script\n",
    "\n",
    "Addition script contains a function which adds two numbers.\n",
    "```\n",
    "\n",
    "save and exit.\n",
    "\n",
    "git should confirm that you have successfully made your first commit.\n",
    "\n",
    "\n",
    "![](../img/git_commit.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note** A commit message is made up of 2 main components:\n",
    "\n",
    "```shell\n",
    "<Title of the commit>\n",
    "\n",
    "<Description of what was done>\n",
    "```\n",
    "\n",
    "- The title should be a description in the form of \"if this commit is applied `<title of the commit>` will happen\". The convention is for this to be rather short and to the point.\n",
    "- The description can be as long as needed and should be a helpful explanation of what is happening.\n",
    "\n",
    "A commit is a snapshot that git makes of your project, you should use this at meaningful steps of the progress of a project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, our file is in the \"Unmodified\" state, because it is identical to the _copy_ that we filed away to our \"file cabinet\" (the location where git stores the snapshots).\n",
    "\n",
    "![image.png](../img/unmodified.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Add and commit the file `addition.py` to git. Write a short but telling commit message."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ignoring files\n",
    "\n",
    "There are still two files in the repository that are currently not being tracted. These are `if-statement.py` and `while-loops.py`. \n",
    "\n",
    "We do not want to keep tract of those files as they are not related to our project.\n",
    "\n",
    "To tell git to ignore these files we will add them to a blank file entitled `.gitignore`.\n",
    "\n",
    "Open your editor and open a new file (`File > New file`) and type:\n",
    "\n",
    "```shell\n",
    "if-statements.py\n",
    "while-loops.py\n",
    "```\n",
    "\n",
    "Save that file as `.gitignore` and then run:\n",
    "\n",
    "```shell\n",
    "$ git status \n",
    "```\n",
    "\n",
    "We see now that `git` is ignoring those 2 files but is aware of the `.gitignore` file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Add and commit the file `.gitignore` to git. Give a good commit message.\n",
    "1. Confirm that you have a clean working directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution:\n",
    "```shell\n",
    "$ git add .gitgignore\n",
    "$ git commit\n",
    "$ git status\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if we run `git status`, we see a message saying that everything in our repository is tracked and up to date."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tracking changes to files\n",
    "\n",
    "\n",
    "Let's assume that we want to refactor (a fancy way of saying \"change\") the function `add_two_numbers` to `add_two_even_numbers` such that the function adds two even numbers but prints a warning if not both numbers are even.\n",
    "\n",
    "## Exercise:\n",
    "1. Change the file `addition.py` to look like this:\n",
    "\n",
    "```python\n",
    "# addition.py\n",
    "def add_two_even_numbers(a, b):\n",
    "    if a % 2 == 0 and b % 2 == 0:\n",
    "        return a + b\n",
    "    else:\n",
    "        print(\"Please use even numbers.\")\n",
    "    \n",
    "print(add_two_even_numbers(4, 6))\n",
    "```\n",
    "\n",
    "1. Save your file and confirm that git detects the change in one file.\n",
    "\n",
    "```shell\n",
    "$ git status\n",
    "```\n",
    "\n",
    "## Solution:\n",
    "![](../img/modified.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our file is now in the modified state, as `git status` tells us.  \n",
    "\n",
    "![image.png](../img/to_modified.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see what has been modified you need to type:\n",
    "\n",
    "```shell\n",
    "$ git diff addition.py\n",
    "```\n",
    "and press `q` to exit.\n",
    "\n",
    "\n",
    "To \"stage\" the file for a commit we use `git add` again:\n",
    "\n",
    "```shell\n",
    "$ git add addition.py\n",
    "```\n",
    "\n",
    "Now let us commit:\n",
    "\n",
    "```shell\n",
    "$ git commit\n",
    "```\n",
    "\n",
    "With the following commit message:\n",
    "\n",
    "```\n",
    "Change add two numbers function to add two even numbers\n",
    "```\n",
    "\n",
    "Finally, we can check the status: \n",
    "```shell\n",
    "$ git status\n",
    "```\n",
    "to confirm that everything has been done correctly.    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exploring history"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`git` allows us to visualize the history of a project and even to change it. To view the history of a repository type:\n",
    "\n",
    "```shell\n",
    "$ git log\n",
    "```\n",
    "\n",
    "This displays the full log of the project:\n",
    "\n",
    "![](../img/git_log.png)\n",
    "\n",
    "\n",
    "We see that there are 3 commits there, each with a seemingly random set of numbers and characters. This set of characters is called a \"hash\".\n",
    "\n",
    "The first commit with title `adds addition script` has hash: aab73629642568b9be5ca5faa5e091ea9a629d67.\n",
    "\n",
    "**Note** that on your machines this hash will be different, in fact every hash is mathematically guaranteed to be unique, thus it is uniquely assigned to the changes made.\n",
    "\n",
    "Hashes can be very useful, e.g. to re-create an earlier state of the project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time travel: Checking out a (previous) commit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `git checkout` commands allows us to revert our file(s) to a state from an earlier commit.\n",
    "Let's try this out and check out the commit just before we made the last change.\n",
    "Find the commit hash to revert to from `git log` first. Highlight the hash and copy it (Ctrl-C). Then paste into the `git checkout` command. It's actually sufficient to take only the first 8 characters from the hash. Remember that your hash will look different from the example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "$ git checkout fbb2cd03\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The message about \"detached HEAD\" means that our file(s) have now reverted to a state from an earlier commit. Any changes to our files would not be commited directy\n",
    "back to our branch (more about branches further below)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The history reported by `git log` now contains only the commits before the checked out commit as you can see by running\n",
    "\n",
    "```shell\n",
    "$ git log\n",
    "```\n",
    "\n",
    "Open the file `addition.py` and confirm that it is again in the state before we changed `add_two_numbers()` to `add_two_even_numbers()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Check out the tip of the branch (aka HEAD)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To go back to the \"tip of the branch\" (the state where we left from before checking out an earlier commit) we run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "$ git checkout master\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Confirm we are back on the master branch:\n",
    "\n",
    "```shell\n",
    "$ git status\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check the history:\n",
    "\n",
    "```shell\n",
    "$ git log\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Checkout the previous commit.\n",
    "1. Confirm that you are on the targeted commit.\n",
    "1. Check that the file `addition.py` contains the ancient code, i.e. before we refactored the code to handle odd and even numbers seperately.\n",
    "1. Go back to the tip of the `master` branch."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating branches\n",
    "\n",
    "Branches allow us to work on different versions of the same file in parallel, which is very important when developing software, but also when we do research."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When typing `git status` we have seen that one piece of information regularly given was:\n",
    "\n",
    "```shell\n",
    "On branch master\n",
    "```\n",
    "\n",
    "This is telling us which branch of \"history\" we are currently on. We can view all branches with the command:\n",
    "\n",
    "```shell\n",
    "$ git branch\n",
    "```\n",
    "\n",
    "This shows:\n",
    "\n",
    "```\n",
    "* master\n",
    "```\n",
    "\n",
    "\n",
    "So currently there is only one branch called master.\n",
    "\n",
    "## Exercise: \n",
    "1. Create a new branch called `implement-add-odd-numbers`.\n",
    "1. Confirm that the new branch has been created.\n",
    "1. Switch (\"checkout\") to the new branch.\n",
    "1. Confirm that you are on the new branch.\n",
    "\n",
    "## Solutions:\n",
    "1. Create branch\n",
    "    ```shell\n",
    "    $ git branch implement-add-odd-numbers\n",
    "    ```  \n",
    "    \n",
    "1. Check branches:\n",
    "    ```\n",
    "    $ git branch\n",
    "    implement-add-odd-numbers\n",
    "    * master\n",
    "    ```\n",
    "\n",
    "1. Switch to the new branch\n",
    "    ```shell\n",
    "    $ git checkout implement-add-odd-numbers\n",
    "    ```\n",
    "    \n",
    "1. Check branch\n",
    "    ```shell\n",
    "    git branch\n",
    "    * implement-add-odd-numbers\n",
    "    master\n",
    "    $ git status\n",
    "    ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Implement a feature in the new branch\n",
    "While we are on this branch we are going to add a new function in the `addition.py` and that is a function that adds two odd numbers.\n",
    "\n",
    "## Exercise:\n",
    "1. Add the following code to `addition.py`:\n",
    "\n",
    "    ```python\n",
    "    def add_two_odd_numbers(a, b):\n",
    "        if a % 2 != 0 and b % 2 != 0:\n",
    "            return a + b\n",
    "        print(\"Please use odd numbers.\")\n",
    "\n",
    "    print(add_two_odd_numbers(1, 3))\n",
    "\n",
    "    ```\n",
    "\n",
    "1. Add and commit the changes to git.\n",
    "1. Checkout the master branch and confirm thate \"addition.py\" is in the old state.\n",
    "1. Switch back to the brach `implement-add-odd-numbers` and confirm the new change has been played back."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solutions:\n",
    "1. Open \"addition.py\" in your editor and apply the changes.\n",
    "\n",
    "1. Add and commit.\n",
    "```shell\n",
    "$ git add addition.py\n",
    "$ git commit \n",
    "```\n",
    "  \n",
    "1. Checkout master\n",
    "```\n",
    "$ git checkout master\n",
    "```\n",
    "    \n",
    "1. List the code in a pager.\n",
    "```shell\n",
    "$ less addition.py\n",
    "```\n",
    "\n",
    "1. Checkout new branch.\n",
    "```shell\n",
    "$ git checkout implement-add-odd-numbers\n",
    "$ less addition.py\n",
    "    ```\n",
    "        \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Merging locally\n",
    "The `git merge` command integrates changes from a branch into the currently checked out branch. Before merging, make sure to have checked out \n",
    "the target branch. In our example we merge changes from the \"implement-add-odd-numbers\" branch into the \"master\" branch."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before merging, always make sure you are on the *target* branch:\n",
    "\n",
    "```shell\n",
    "$ git checkout master\n",
    "\n",
    "Switched to branch 'master'\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the `git merge` command to bring in the changes from the \"implement-add-odd-numbers\" branch.\n",
    "It is good practice to always append the `--no-ff` flag to `git merge` to create a dedicated merge commit. In this way one can later identify changes in the code that are a result from a merge."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "$ git merge implement-add-odd-numbers --no-ff \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In your editor, the commit message will already be present, you can leave it as is or add more detailed information. Save and close the editor."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Confirm that the changes from the branch `implement-add-odd-numbers` are properly merged into the master branch.\n",
    "\n",
    "## Solutions:\n",
    "1.\n",
    "```\n",
    "$ git log\n",
    "```\n",
    "\n",
    "Pro tip: To visualize the tree structure of branches and commits, use the following `git log` command:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "$ git log --pretty=oneline --graph --abbrev-commit\n",
    "\n",
    "*   c1ba40d (HEAD -> master) Merge branch 'implement-add-odd-numbers'\n",
    "|\\  \n",
    "| * fadd60e (implement-add-odd-numbers) implement function for adding odd numbers\n",
    "|/  \n",
    "* d80f1c9 change add two numbers function to add two even numbers\n",
    "* 3fca9d1 Add .gitignore\n",
    "* 8487ec4 Add addition script\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Delete feature branch \n",
    "If we are certain that the feature branch `implement-add-odd-numbers` will no longer be used, we can delete it:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "$ git branch -D implement-add-odd-numbers\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "1. Confirm that the feature branch is deleted."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution:\n",
    "```shell\n",
    "$ git branch\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Merge conflicts (TODO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusions\n",
    "\n",
    "This is the end of Day 1 of our workshop. Here's what you learned today:\n",
    "\n",
    "* Using the command line to navigate the directory tree and create directories.\n",
    "* First steps in python programming\n",
    "* Using git for version control: \n",
    "    - The git cycle: `add`, `commit`, `push`\n",
    "    - Going back and forth in history\n",
    "    - Branching: list, create, merge, delete"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Outlook\n",
    "On day 2 of the workshop, we will cover:\n",
    "* Best practices for software development: Documentation and testing\n",
    "* Git in the cloud: GitLab, Merge requests, `git push` and `git pull`\n",
    "* Continuous integration: Automated testing\n",
    "* Gitlab pages: Publish your online reference manual"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
