# Research Software Development workshop

[![pipeline status](https://gitlab.gwdg.de/mpievolbio-scicomp/rsd-workshop/badges/book/pipeline.svg)](https://gitlab.gwdg.de/mpievolbio-scicomp/rsd-workshop/-/commits/book) 

To read the course material as a jupyter book, go to https://mpievolbio-scicomp.pages.gwdg.de/rsd-workshop.

This repository contains material for running a research software development (rsd)
workshop.

The material is based on a previous rsd workshop which we designed with [@drvinceknight](https://twitter.com/drvinceknight)
& [@GeraintPalmer](https://twitter.com/GeraintPalmer). The material for the
previous workshop are available at https://vknight.org/rsd/.


# Installation

This workshop is written in [Jupyter Notebooks](https://jupyter.org), however,
they are not required for the workshop.

The requirements with installation notes can be found at [Installation [pre workshop]](https://github.com/Nikoleta-v3/rsd-workshop/blob/master/Installation%20%5Bpre%20workshop%5D.ipynb).

# Topics

The workshop covers:

- Basic use of the command line
- Version control with git
- Best practice in writing computer code (the workshop uses Python but the ideas are applicable to any language)
- Setting up a repository in GitHub with license, tests, README, etc


# Usage

The material is designed for a two days workshop. Suggested timetable:

**Day I**

- 11:00 - 11:15 Introduction, check for installations
- 11:15 - 11:30 An introduction to command line
- 11:15 - 12:00 An introduction to Python
- 12:00 - 13:00 Lunch
- 13:00 - 15:30 More python
- 15:30 - 16:00 Best practices discussion

**Day II**

- 11:00 - 12:00 Introduction to git
- 12:00 - 13:00 Lunch
- 13:00 - 14:00 Git exercise I (add, commit, diff, branch)
- 14:00 - 16:00 More git (merge, conflicts, push)
- 16:00 - 17:00 gitlab: CI, README, merge request

# Contributions

All contributions are welcome! This may include communicating ideas for new
sections, letting us know about bugs, and code contributions.

# License

The code in this repository, including all code samples in the notebooks listed
above, is released under the MIT license.

# Further reading

A number of sources that provide excellent guidance and references on the
development of scientific software:

- ["Share and Enjoy": Publishing Useful and Usable Scientific Models](https://arxiv.org/abs/1409.0367)
- ["Top Tips to Make Your Research Irreproducible"](https://arxiv.org/abs/1504.00062)
- ["Ten Simple Rules for the Open Development of Scientific Software"](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002802)
- ["Re-run, Repeat, Reproduce, Reuse, Replicate: Transforming Code into Scientific Contributions"](https://arxiv.org/abs/1708.08205)
- ["Gene name errors are widespread in the scientific literature."](https://pubmed.ncbi.nlm.nih.gov/27552985/)
- ["A Guide to Reproducible Code in Ecology and Evolution"](https://www.britishecologicalsociety.org/wp-content/uploads/2017/12/guide-to-reproducible-code.pdf)

# Jupyter Book

The material is being moved to [Jupyter-Books](https://jupyterbook.org/intro.html).

## Build

To compile the book locally you need to install `jupyter-book`. This repository comes with an `environment.yml` file. To create the environment using `conda` run:

```shell
$ conda env create -f environment.yml
```

To activate the environment:

```shell
$ conda activate rsd
```

Once the environment is activated to build the HTML for the book run the following
command:

```
$ jupyter-book build book/
```

## Local preview

To preview the book, you can open the generated HTML files (under `_build/`) in your browser.
Either double-click the html file in your local folder, or enter the absolute path to the file in your browser navigation bar adding `file://` at the beginning 
(e.g. `file://Users/path/rsd-workshop/book/_build/index.html`).

