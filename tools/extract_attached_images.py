import nbconvert
import nbformat
import sys
import base64

with open(sys.argv[1]) as nb_file:
    nb_contents = nb_file.read()

# Convert using the ordinary exporter
notebook = nbformat.reads(nb_contents, as_version=4)

# Create a dict mapping all image attachments to their base64 representations
images = {}
for cell in notebook['cells']:
    if 'attachments' in cell:
        attachments = cell['attachments']
        for filename, attachment in attachments.items():
            for mime, base64str in attachment.items():
                bytes_ = bytes(base64str, 'utf-8')
                
                with open(filename, 'wb') as img:
                    img.write(base64.decodebytes(bytes_))